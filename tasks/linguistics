Task: Linguistics
Install: false
Description: Debian Science Linguistics packages
 This metapackage is part of the Debian Pure Blend "Debian Science"
 and installs packages related to Linguistics.

Recommends: wordnet

Recommends: apertium, apertium-lex-tools, link-grammar

Recommends: artha

Recommends: sequitur-g2p

Recommends: timbl, timblserver, mbt, mbtserver, ucto, dimbl, frog

Suggests: libticcutils2-dev

Suggests: frogdata

Suggests: libtimbl3-dev, libtimbl4-dev, libtimblserver2-dev, libmbt0-dev, libucto1-dev, libfolia-dev

Recommends: collatinus

Recommends: python3-timbl
WNPP: 705129
Responsible: Maarten van Gompel <proycon@anaproy.nl>
Homepage: http://github.com/proycon/python-timbl/
Pkg-Description: Python bindings for the Tilburg Memory Based Learner (Timbl)
 python-timbl is a Python extension module wrapping the full TiMBL C++
 programming interface. With this module, all functionality exposed
 through the C++ interface is also available to Python scripts. Being
 able to access the API from Python greatly facilitates prototyping
 TiMBL-based applications.
 .
 TiMBL is an open source software package implementing several
 memory-based learning algorithms, among which IB1-IG, an
 implementation of k-nearest neighbor classification with feature
 weighting suitable for symbolic feature spaces, and IGTree, a
 decision-tree approximation of IB1-IG. All implemented algorithms have
 in common that they store some representation of the training set
 explicitly in memory. During testing, new cases are classified by
 extrapolation from the most similar stored cases.
 .
 The Python module offers both a high-level as well as a low-level
 interface, the former is very Pythonic and easy to use while the
 latter offers the full API.

Suggests: wnsqlbuilder
Homepage: http://wnsqlbuilder.sf.net
License: GPL
Pkg-Description: SQL version of WordNet 3.0
 WordNet SQL Builder is a Java utility to generate SQL database from
 WordNet standard database as released by the WordNet Project (Princeton
 University)
 .
 Features
  * Support for MySql and PostGreSQL.
  * Complete port (however, orphaned morphological forms are dropped, and
    so are VerbNet/XWordNet data that cannot be linked to WordNet entries).
  * Incremental build support.
  * Retains synset index as primary key allowing easy reference to wordnet
    original database
  * Includes support for WordNet 3.0
  * Includes support for WordNet 2.0 to 2.1, 2.1 to 3.0, 2.0 to 3.0 sense maps
  * Includes support for VerbNet 2.3
  * Includes support for XWordNet 2.0-1.1
  * Ready-to-use database (see wnsqldatabase package in download section) including
    - WordNet 3.0
    - WordNet 2.0 to 2.1, 2.1 to 3.0, 2.0 to 3.0 sense maps
    - VerbNet 2.3
    - XWordNet 2.0-1.1
    - British National Corpus statistical data (for commonly used-words)

Recommends: travatar

Suggests: python3-nltk

Suggests: apertium-af-nl,
          apertium-apy,
          apertium-arg,
          apertium-arg-cat,
          apertium-bel,
          apertium-bel-rus,
          apertium-br-fr,
          apertium-ca-it,
          apertium-cat,
          apertium-cat-srd,
          apertium-crh,
          apertium-crh-tur,
          apertium-cy-en,
          apertium-dan,
          apertium-dan-nor,
          apertium-en-ca,
          apertium-en-es,
          apertium-en-gl,
          apertium-eo-ca,
          apertium-eo-en,
          apertium-eo-es,
          apertium-eo-fr,
          apertium-es-ast,
          apertium-es-ca,
          apertium-es-gl,
          apertium-es-it,
          apertium-es-pt,
          apertium-es-ro,
          apertium-eu-en,
          apertium-eu-es,
          apertium-fra,
          apertium-fra-cat,
          apertium-fr-ca,
          apertium-fr-es,
          apertium-hbs,
          apertium-hbs-eng,
          apertium-hbs-mkd,
          apertium-hbs-slv,
          apertium-hin,
          apertium-id-ms,
          apertium-isl,
          apertium-isl-eng,
          apertium-is-sv,
          apertium-ita,
          apertium-kaz,
          apertium-kaz-tat,
          apertium-mk-bg,
          apertium-mk-en,
          apertium-mlt-ara,
          apertium-nno,
          apertium-nno-nob,
          apertium-nob,
          apertium-oc-ca,
          apertium-oc-es,
          apertium-oci,
          apertium-pol,
          apertium-pt-ca,
          apertium-pt-gl,
          apertium-rus,
          apertium-separable,
          apertium-sme-nob,
          apertium-spa,
          apertium-spa-arg,
          apertium-srd,
          apertium-srd-ita,
          apertium-swe,
          apertium-swe-dan,
          apertium-swe-nor,
          apertium-szl,
          apertium-tat,
          apertium-tur,
          apertium-ukr,
          apertium-urd,
          apertium-urd-hin,
          python3-streamparser

Recommends: cg3

Suggests: libcg3-dev

Recommends: libcld2-dev

Suggests: r-cran-nlp, r-cran-tm

Recommends: python3-pynlpl

Recommends: uctodata

Recommends: giella-sme

Recommends: hfst, hfst-ospell, lttoolbox

Suggests: python3-snowballstemmer

Recommends: r-cran-snowballc, r-cran-lexrankr

Recommends: spacy

Recommends: python3-thinc

Recommends: irstlm
